package com.finaltest.thread;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

public class ThreadTest {
	
	private static final Logger logger = LogManager.getLogger();
	
	public void createThreads() {
		Runnable r1 = ()-> {ThreadContext.put("name", "Adrian Cazares"); ThreadContext.put("userName", "adrian"); logger.fatal("test lambda fatal");};
		Thread t1 = new Thread(r1, "Thread 1");
		t1.start();
		
		Runnable r2 = ()-> {ThreadContext.put("name", "Juan Castro"); ThreadContext.put("userName", "juan"); logger.error("test lambda error");};
		Thread t2 = new Thread(r2, "Thread 2");
		t2.start();
		
		Runnable r3 = ()-> {ThreadContext.put("name", "Guadalupe Martinez"); ThreadContext.put("userName", "lupe"); logger.warn("test lambda warn");};
		Thread t3 = new Thread(r3, "Thread 3");
		t3.start();		
		
		Runnable r4 = ()-> {ThreadContext.put("name", "Oscar Garcia"); ThreadContext.put("userName", "oscar"); logger.info("test lambda info");};
		Thread t4 = new Thread(r4, "Thread 4");
		t4.start();	
		
		Runnable r5 = ()-> {ThreadContext.put("name", "Maria Perez"); ThreadContext.put("userName", "maria"); logger.debug("test lambda debug");};
		Thread t5 = new Thread(r5, "Thread 5");
		t5.start();	
		
		Runnable r6 = ()-> {ThreadContext.put("name", "Rosa Hernandez"); ThreadContext.put("userName", "rosa"); logger.trace("test lambda trace");};
		Thread t6 = new Thread(r6, "Thread 6");
		t6.start();			
	}

}
