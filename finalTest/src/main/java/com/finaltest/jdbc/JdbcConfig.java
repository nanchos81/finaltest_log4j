package com.finaltest.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

import org.apache.commons.dbcp2.BasicDataSource;

public class JdbcConfig {
	private BasicDataSource dataSource;
	private JdbcConfig() {}
	
	public Connection getConnection() throws SQLException, ClassNotFoundException {
		if(dataSource == null) {
			dataSource.setDriverClassName("org.postgresql.Driver");
			dataSource.setUrl("jdbc:postgresql://localhost:5432/postgres");
			dataSource.setUsername("postgres");
			dataSource.setPassword("postgres");
		}

		return dataSource.getConnection();

		
	}
}
